# Kubernetes Certified Administrator & Application Developer Notes

I use this repository to study Kubernetes, mostly when I am reading the official Kubernetes's documentation. When I started to study for the CKA exam I created this repository with the only intention to have a single place to record the process. 

## Certifications

- Certified Kubernetes Administrator (CKA) [Done 2020]

## Certifications Plan

- Certified Kubernetes Application Developer (CKAD) [In Progress 2022]
- Certified Kubernetes Security Specialist (CKS) [Scheduled for June 2023]
- Certified Kubernetes Administrator (CKA) Recertification [December 2023]

## Certified Kubernetes Application Developer

Simple process:

- https://kubernetes.io/docs/home/
- https://github.com/dgkanatsios/CKAD-exercises
- https://github.com/ahmetb/kubernetes-network-policy-recipes
- https://killer.sh/

## Certified Kubernetes Security Specialist

- https://kubernetes.io/docs/home/
- https://www.udemy.com/course/certified-kubernetes-security-specialist/
- https://www.udemy.com/course/certified-kubernetes-security-specialist-certification/
- https://saiyampathak.gumroad.com/l/cksbook
- https://github.com/walidshaari/Certified-Kubernetes-Security-Specialist
- https://killer.sh

## Things to study

- Entrypoints & Commands
- Volume management
- Kubernetes documentation (*)
  - Concepts
    - Workloads
      - Pods
      - Workload Resources
